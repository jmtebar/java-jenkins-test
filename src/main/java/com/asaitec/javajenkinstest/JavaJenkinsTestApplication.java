package com.asaitec.javajenkinstest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaJenkinsTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaJenkinsTestApplication.class, args);
	}

}
